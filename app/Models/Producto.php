<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Producto
 *
 * @property $id
 * @property $codigo
 * @property $nombre
 * @property $presentacion
 * @property $precio
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Producto extends Model
{
    
    static $rules = [
		'codigo' => 'required',
		'nombre' => 'required',
		'presentacion' => 'required',
		'precio' => 'required',
    'cantidad' => 'required',
    'estado' => 'required',
    'observaciones' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['codigo','nombre','presentacion','precio','cantidad','estado','observaciones'];



}
